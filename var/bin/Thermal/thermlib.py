#!/usr/bin/python
import os
import glob

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

def read_temp_raw(tbus="28*"): #returns the sensor data
	base_dir = '/sys/bus/w1/devices/'
	device_folder = glob.glob(base_dir + tbus)[0]
	device_file = device_folder + '/w1_slave'
	f = open(device_file, 'r')
	lines = f.readlines()
	f.close()
	return lines

def read_temp(tbus="28*"): 	#reads temp returns a float in C
	lines = read_temp_raw(tbus)
	while lines[0].strip()[-3:] != 'YES':
		lines = read_temp_raw()
	equals_pos = lines[1].find('t=')
	if equals_pos != -1:
		temp_string = lines[1][equals_pos+2:]
		temp_c = float(temp_string) / 1000.0
		return temp_c
		
def ctof(temp_c): #converts from C to F
	temp_f = temp_c * 9.0 / 5.0 + 32.0
	return temp_f
	
def tempcfloat(tbus="28*"): #returns Temp in C as float 
	return read_temp(tbus)[0]
	
def tempffloat(tbus="28*"): #returns Temp in F as float 
	return (ctof(read_temp(tbus)))
	
def tempctenths(tbus="28*"): #returns Temp in C as float rounded to .1
	return str(round((read_temp(tbus)),1))
	
def tempftenths(tbus="28*"): #returns Temp in F as float rounded to .1
	return str(round((ctof(read_temp(tbus))),1))
	
def tempcint(tbus="28*"): #returns Temp in C as integer
	return str(int(round((read_temp(tbus)),0)))
	
def tempfint(tbus="28*"): #returns Temp in F as integer
	return str(int(round((ctof(read_temp(tbus))),0)))
	
def tempcstr(tbus="28*"): #returns Temp in C as integer string
	return str(int(round((read_temp(tbus)),0)))
	
def tempfstr(tbus="28*"): #returns Temp in F as integer string
	return str(int(round((ctof(read_temp(tbus))),0)))