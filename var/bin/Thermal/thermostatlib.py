#!/usr/bin/python

import os
import math

def settemp():
	readtemp = open("/var/bin/thermo/thermostat", "r")
	setemp = readtemp.readline()
	readtemp.close()
	#print "in settemp " + setemp
	return int(setemp)
	
def holdtemp(currtemp):
	tempsetting = int(settemp())
	if int(currtemp) >= (tempsetting + .1):
		status = open("/var/bin/thermo/status", "w")
		status.write("Furnace is off.")
		status.close()		
		#print "Off loop " + str(currtemp)
		return int(0)
	if int(currtemp) <= tempsetting:
		status = open("/var/bin/thermo/status", "w")
		status.write("Furnace is ON.")
		status.close()
		#print "ON loop " + str(currtemp)
		return int(1)