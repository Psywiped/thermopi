#!/usr/bin/python

import pylcdlib
import thermlib
import time
import math
import RPi.GPIO as GPIO
import thermostatlib


from subprocess import * 
from time import sleep, strftime
from datetime import datetime
pitempsensor="28-000003ce403a"
lcd = pylcdlib.lcd(0x20,1,1)

GPIO.setwarnings(False)
GPIO.cleanup()
GPIO.setmode(GPIO.BCM)
# set BCM pin number for the furnace.
FURNACE = 18
GPIO.setup(FURNACE, GPIO.OUT)
GPIO.output(FURNACE, 0)

# wlan = "ip addr show wlan0 | grep inet | awk '{print $2}' | cut -d/ -f1"
wlan = "ifconfig wlan0 | grep inet | awk '{print $2}' | cut -d: -f2"
eth = "ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1"

def run_cmd(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	output = p.communicate()[0]
	#print repr(output)
	return output
while 1:
	pitemp = str(thermlib.tempfstr(pitempsensor))
	f = open('/var/bin/thermo/walltemp','w')
	f.write(pitemp)
	f.close()	
 	line1 = datetime.now().strftime('%b %d %H:%M') + ' ' + pitemp + 'F'
	wlanip = run_cmd(wlan)
	ethip = run_cmd(eth)
	line2 = "WIFI " + wlanip
	line3 = 'Eth0  ' + ethip
	lcd.lcd_clear()
	heat = thermostatlib.holdtemp(pitemp)
	GPIO.output(FURNACE, heat)
	print "main loop"
	if heat == 1:
		line4 = "Heat ON set @ " + str(thermostatlib.settemp()) + "F"
	if heat == 0:
		line4 = "Heat off set @ " + str(thermostatlib.settemp()) + "F"
#	print line1
#	print line2
#	print line3 + '\n\n'
	lcd.lcd_puts(line1,1)
	lcd.lcd_puts(line2,2)
	lcd.lcd_puts(line3,3)
	lcd.lcd_puts(line4,4)
	sleep(5)

