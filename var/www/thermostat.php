<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
        <title>HotPi</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <meta name="Thermostat" content="HotPi Thermostat" />
    </head>
<?php
if(isset($_POST["settemp"])) {

$settemp = $_POST["settemp"];
$fp = fopen("/var/bin/thermo/thermostat", "w+");
$savestring = $settemp;
fwrite($fp, $savestring);
fclose($fp);
}
?>
<body>
	<header>
        <div class="logo">
            <a href="index.php">
                <img src="images/logo.png" alt="HotPi Thermostat" />
            </a>
        </div>
		<div class="clear"></div>
    </header>
    <div class="content">
    <article>
    	<h2 class="underline">Thermostat</h2>
		 <div style='width: 240px; height: 420px; background-image: url( http://vortex.accuweather.com/adcbin/netweather_v2/backgrounds/black_240x420_bg.jpg ); background-repeat: no-repeat; background-color: #000000;' ><div id='NetweatherContainer' style='height: 405px;' ><script src='http://netweather.accuweather.com/adcbin/netweather_v2/netweatherV2ex.asp?partner=netweather&tStyle=whteYell&logo=1&zipcode=60012&lang=eng&size=12&theme=black&metric=0&target=_self'></script></div><div style='text-align: center; font-family: arial, helvetica, verdana, sans-serif; font-size: 10px; line-height: 15px; color: #FFFFFF;' ><a style='font-size: 10px; color: #FFFFFF' href='http://www.accuweather.com/us/il/crystal-lake/60012/city-weather-forecast.asp?partner=accuweather' >Weather Forecast</a> | <a style='color: #FFFFFF' href='http://www.accuweather.com/maps-satellite.asp' >Weather Maps</a> | <a style='color: #FFFFFF' href='http://www.accuweather.com/index-radar.asp?partner=accuweather&zipcode=60012' >Weather Radar</a></div></div>
<h5>Main floor temperature is <?php $curSet = file_get_contents('/var/bin/thermo/walltemp'); echo $curSet; ?>°F. <?php $onoff = file_get_contents('/var/bin/thermo/status'); echo $onoff; ?></h5>

				<form name="termostat" method="post" class="label-top" action="<?php echo $_SERVER['PHP_SELF']; ?>">
				<div>
					<label for="settemp" class="inline">Temperature set at</label>
					<select name="settemp" id="settemp" onChange="this.form.submit()">
						<option value=null SELECTED><?php $curSet = file_get_contents('/var/bin/thermo/thermostat'); echo $curSet; ?> °F </option>
						<option value="80">High(80°F) </option>
						<option value="76">76°F</option>
						<option value="74">74°F</option>
						<option value="72">72°F</option>
						<option value="70">Home(70°F)</option>
						<option value="68">68°F</option>
						<option value="65">65°F</option>
						<option value="55">Away(55°F)</option>
						<option value="45">Low(45°F)</option>
					</select>
				</div>
				</form>
    <footer>
        <nav class="vertical menu">
            <ul>
                <li><a href="index.php">Main</a></li>
            </ul>
        </nav> 
    </footer>
</body>
</html>
